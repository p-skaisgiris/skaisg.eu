+++
title = "Most representative song of course participants"
date = 2021-03-26
[extra]
tags = ["all", "python", "various music APIs (e.g. spotify)", "seaborn"]
+++

<i class="fa fa-video-camera"></i> [Video](https://www.youtube.com/watch?v=AU491GOg7No)

We wanted to figure out the most representitive song of our course. As the primary dataset we used the YouTube music playlist that was shared among students attending the Data Analysis course. Everyone could freely edit it. We used many music APIs to craft a dataset that had interesting features of the songs appearing in the playlist such as tempo, key, danceability, etc. We explored the data and present our findings in the aforementioned video.
