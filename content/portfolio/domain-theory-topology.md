+++
title = "Domain theory and its topological connections"
date = 2023-02-01
[extra]
tags = ["all", "theory", "denotational semantics", "lattice theory", "topology"]
+++

<i class="fa fa-desktop" aria-hidden="true"></i> [Presentation](/pdfs/topology-presentation.pdf)

Researched and succinctly summarized an introduction to Domain Theory for MSc Logic students as part of our [Topology In and Via Logic](https://msclogic.illc.uva.nl/current-students/courses/projects/project/206/1st-Semester-2022-23-Topology-in-and-via-Logic) course. In the presentation we put some emphasis on the
