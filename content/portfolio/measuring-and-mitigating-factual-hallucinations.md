+++
title = "Measuring and mitigating factual hallucinations for text summarization"
date = 2023-10-14
[extra]
tags = ["all", "software", "python", "pytorch", "natural language processing"]
+++

<i class="fa fa-file-text"></i> [Report](/pdfs/dl4nlp-hallucination-project.pdf)

Advancements in NLG have improved text generation quality but still suffer from hallucinations, which lead to irrelevant information and
factual inconsistencies. This paper provides an ensemble of metrics that measure whether the generated text is factually correct. Using these metrics we find that fine-tuning is a fruitful hallucination mitigation approach whilst prompt engineering is not.
