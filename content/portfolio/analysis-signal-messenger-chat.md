+++
title = "Analysis of Signal Messenger chat for relationship anniversary"
date = 2020-03-14
[extra]
tags = ["all", "natural language processing", "adobe xd", "python", "matplotlib", "nltk"]
+++

<i class="fa fa-reddit"></i> [Reddit post](https://www.reddit.com/r/dataisbeautiful/comments/gdduzf/oc_i_made_this_cyberpunk_style_poster_for_my/)

{{ captioned(src="/images/portfolio/poster-single-page.png", alt="Cyberpunk style poster showing chat analysis", width="800") }}
