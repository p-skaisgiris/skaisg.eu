+++
title = "Investigating the cross-lingual sharing mechanism of multilingual models through their subnetworks "
date = 2024-06-01
[extra]
tags = ["all", "software", "python", "pytorch", "natural language processing"]
+++

<i class="fa fa-github"></i> [Repository](https://github.com/MaxBelitsky/cross-lingual-subnetworks)

We investigate to what extent computation is shared across languages in multilingual models. In particular, we prune language-specific subnetworks from a multilingual model and check how much overlap there is between subnetworks for different languages and how these subnetworks perform across other languages. We also measure the similarity of the hidden states of a multilingual model for parallel sentences in different languages. All of these experiments suggest that a substantial amount of computation is being shared across languages in the multilingual model we use (XLM-R).
