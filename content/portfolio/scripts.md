+++
title = "Scripts customizing my personal computer experience"
date = 2020-01-01
[extra]
tags = ["all", "software"]
+++

<i class="fa fa-gitlab"></i> [Repository](https://gitlab.com/p-skaisgiris/i3-dot-files)

I use [i3 window manager](https://i3wm.org/) and spent quite a few hours making it look nice and comfy.

{{ captioned(src="/images/portfolio/i3.jpg", alt="Screenshot of my i3 window manager setup", width="800") }}

<i class="fa fa-gitlab"></i> [Repository](https://gitlab.com/p-skaisgiris/cli-sleep-tracker-visualizer)

This little Julia script allows me to easily answer the question "How well am I sleeping?".

{{ captioned(src="/images/portfolio/sleep-tracker.jpg", alt="Screenshot of my cli-sleep-tracker script", width="800") }}

<i class="fa fa-gitlab"></i> [Repository](https://gitlab.com/p-skaisgiris/cli-eisenhower)

I like keeping track of the tasks I need to do and which ones are most important and urgent. I like it even more when that visualization is minimal and in the terminal.

{{ captioned(src="/images/portfolio/eisenhower.jpg", alt="Screenshot of cli-eisenhower script", width="800")}}
