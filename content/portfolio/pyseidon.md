+++
title = "PySeidon - A Data-Driven Maritime Port Simulation Framework"
date = 2021-06-01
[extra]
tags = ["all", "publication", "software", "python", "geoplotlib", "geojson", "esper", "fysom"]
+++

<i class="fa fa-gitlab"></i> [Repository](https://github.com/pyseidon-sim/pyseidon) <i class="fa fa-file-text"></i> [Paper](https://dl.acm.org/doi/abs/10.1145/3474963.3474986)

Extendable and modular software for maritime port simulation. PySeidon can be used to explore the effects of a decision introduced in a port, perform scenario testing, approximate Key Performance Indicators of some decision, create new data for various downstream tasks (e.g. anomaly detection).

Software project born as part of [MaRBLe 2.0](https://www.maastrichtuniversity.nl/research/dke/honours-programme).
