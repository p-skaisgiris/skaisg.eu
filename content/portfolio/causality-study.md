+++
title = "Causality study - how social networks influence one's decision to insure"
date = 2023-12-22
[extra]
tags = ["all", "python", "software", "causality", "dowhy", "networkx"]
+++

<i class="fa fa-github"></i> [Repository](https://github.com/Dawlau/rice-insurance-causality)

We look at an experiment that investigated how the social environment of rice farmers in rural China influences whether they purchase weather insurance, along with other variables such as demographics or whether they have previously adopted weather insurance.
