+++
title = "Bridging statistical learning theory and dynamic epistemic logic"
date = 2024-06-05
[extra]
tags = ["all", "theory", "logic", "learning theory"]
+++

<i class="fa fa-file-text"></i> [Paper](/pdfs/slt-to-del-via-flt.pdf)

This paper establishes a connection between statistical learning theory (SLT) and formal learning theory (FLT). We use this insight to connect SLT and dynamic epistemic logic (DEL) models via the already established FLT and DEL bridge due to [Gierasimczuk, 2009]. Specifically, we demonstrate that the uniform convergence property of a hypothesis space implies the finite identifiability of its corresponding epistemic space which, in turn, can be modelled in DEL [Gierasimczuk, 2009]. This paper thus lays the foundations of an alternative way to introduce probabilistic reasoning into DEL, reason about statistical learning scenarios topologically, and investigate the epistemology of statistical learning theory.
