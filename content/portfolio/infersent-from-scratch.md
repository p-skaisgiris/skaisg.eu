+++
title = "InferSent sentence representations from scratch"
date = 2024-04-21
[extra]
tags = ["all", "software", "python", "pytorch", "natural language processing"]
+++

<i class="fa fa-gitlab"></i> [Repository](https://gitlab.com/p-skaisgiris/infersent-from-scratch)

This repository is a modular and extendable PyTorch Lightning re-implementation of InferSent sentence representations by [Conneau et al., 2017](https://arxiv.org/abs/1705.02364).
We train four types of sentence encoders - average, unidirectional LSTM, bidirectional LSTM, bidirectional LSTM with max pooling - via the natural language inference task using the SNLI dataset. This is done to create sentence representations which are general and transferable to other tasks. Refer to the paper for more information.
