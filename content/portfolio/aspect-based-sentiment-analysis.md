+++
title = "Aspect-Based Sentiment Analysis"
date = 2020-05-26
[extra]
tags = ["all", "software", "python", "natural language processing", "pytorch", "tensorflow", "textblob", "spacy"]
+++

<i class="fa fa-gitlab"></i> [Repository](https://gitlab.com/p-skaisgiris/ina) <i class="fa fa-paragraph"></i> [Blog post](https://p-skaisgiris.gitlab.io/ina-website/ina/)

I built a tool that analyzes the comments under a New York Times article for my Natural Language Processing course. Specifically, it finds aspects (topics) in a sentence and predicts the sentiment associated with them. This approach allows to get a more nuanced opinion regarding various sentiments mentioned in sentences, not just the average sentiment of the sentence.
