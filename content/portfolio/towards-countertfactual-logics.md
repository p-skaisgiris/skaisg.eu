+++
title = "Towards counterfactual logics for machine learning"
date = 2023-08-29
[extra]
tags = ["all", "theory", "python", "logic", "machine learning"]
+++

<i class="fa fa-file-text"></i> [Paper](/pdfs/towards-counterfactual-logics.pdf)

In this project, I have explored the connections between the informal, but practical, counterfactual explanation generation method and the formal semantic frameworks for counterfactuals. Specifically, I described a restricted notion of soundness and completeness and attempted to prove it for CE generation and variation semantics. I have succeeded in proving soundness but not completeness. Lastly, this paper provides ideas for further investigations in this area, and intuitions how completeness may be still achieved.
