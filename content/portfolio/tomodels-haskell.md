+++
title = "Topomodels in Haskell"
date = 2023-06-04
[extra]
tags = ["all", "software", "haskell", "modal logic", "topology"]
+++

<i class="fa fa-gitlab"></i> [Repository](https://gitlab.com/dqalombardi/topomodels) <i class="fa fa-file-text"></i> [Report](https://gitlab.com/dqalombardi/topomodels/-/blob/main/docs/report.pdf) <i class="fa fa-desktop" aria-hidden="true"></i> [Presentation](https://gitlab.com/dqalombardi/topomodels/-/blob/main/docs/presentation.pdf)

We provide a library for working with general topological spaces as well
as topomodels for modal logic. We also implement a well-known construction for converting
topomodels to S4 Kripke models and back. Furthermore, we developed correctness tests and
benchmarks which are meant to be extended by users. Thus, this work serves as a solid
starting point for investigating hypotheses about general topology, modal logic, and their
intersection.
