+++
title = "BSc Thesis - Formal Verification of Deep Neural Networks for Sentiment Classification"
date = 2021-06-27
[extra]
tags = ["all", "software", "python", "pytorch", "natural language processing", "julia", "NeuralVerification.jl"]
+++

<i class="fa fa-gitlab"></i> [Repository](https://gitlab.com/p-skaisgiris/sent-verification) <i class="fa fa-file-text"></i> [Thesis](/pdfs/bsc-thesis.pdf) <i class="fa fa-desktop" aria-hidden="true"></i> [Presentation](/pdfs/thesis-presentation.pdf)

I conducted research on verification, a technique that guarantees certain properties in neural networks. My focus was on the effectiveness of existing verification frameworks used in networks for sentiment classification, given the limited research in concrete NLP verification. Additionally, I explored the latent space attributes of various text representation methods. My findings reveal that the latent space created by an autoencoder, trained using a denoising adversarial objective, is effective for confirming the robustness of networks engaged in sentiment classification and for interpreting the outcomes of verification tools.
