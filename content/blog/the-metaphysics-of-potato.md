+++
title = "The Metaphysics of Potato or The Precursor to Alpha and Omega"
date = 2021-04-09
+++

To me, the most memorable introduction to any book is that of Lithuanian Philosopher's Arvydas Šliogeris's "The Methaphysics of Potato" (Bulvės Metafizika). 

As I sat down to write this, I have just finished a slice of rye bread. It reminded me of a hot summer's day, of a time when I was tasked by my father to hoe a dozen of rows of potatoes, of the earth's fragrance as I was opening and closing its innards.

I thus present a clumsy translation of that introduction for which I can only blame Google Translate and my poor understanding of the text.

...You are the immortal Teachers of Humanity;\
You are the great prophets;\
You are the agents of "God";\
You are the builders of the towers of Babel;\
You are the witnesses of the Big Bang;\
You are the dinosaur shepherds;\
You are the stars of postmodern Globalopolis;\
You are the captains of the ships of Eternity;\
You are the tamers of the infinite winds;\
You are light-year surveyors, galaxy hosts, and antimatter sculptors;\
You are the Mahatmas, Buddhas, Muhammads, and saviors;\
You are the helmsmen of history, the architects of the concentration camps and the Holocaust;\
You are shamans, smiths of Immortality, and omniscients;\
You are Sai Babas, angelic doctors, Hegelians, Zaratustras, Galileans and Marxes;\
You are the castrates of Alexandrias and the androgens of Paris;\
You are the gods of laboratories, genetic engineers, nanotechnologists and stem cell hunters;\
I want to tell you what I saw once in Nataliukė's garden, in the church village of Krosna, digging a black, fragrant earth with my father's hoe. You won’t believe it - I saw a POTATO, a white oval with black dots, in the center of the oval I saw Everything and even more than Everything, and for a moment - just a moment - I became a real visionary of Aleph - a potato seer. Could it be that the last avatar of clairvoyance becomes potato sight?
