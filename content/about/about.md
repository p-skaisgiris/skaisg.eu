+++
title = "About"
date = 2024-04-04
+++

<img src="/images/about/me-stencil.png" alt= "Picture of the author of this site" align="right" width="200" height="200" style="padding: 20px; margin-right:30px" onclick=toggleColor(this) >

Hello, my name is Paulius. I am currently studying [MSc Logic](https://msclogic.illc.uva.nl/programme/mol/) at the [Institute for Logic, Language and Computation](https://www.illc.uva.nl/) in [University of Amsterdam](https://www.uva.nl/en). Previously, I have worked as a Machine Learning Engineer at [IconPro](https://www.iconpro.com/en/home/). Before that, I studied at [DKE](https://www.maastrichtuniversity.nl/research/department-data-science-and-knowledge-engineering-dke) in [Maastricht University](https://www.maastrichtuniversity.nl/) and obtained a BSc with Honours in [Data Science and Artificial Intelligence](https://www.maastrichtuniversity.nl/education/bachelor/data-science-and-artificial-intelligence). At DKE I was also the President of Study Association [MSV Incognito](https://msvincognito.nl/boards/).

I created this little site in an effort to concretize my thoughts about some topics and store previews of projects I have worked on.

You can find me on [LinkedIn](https://www.linkedin.com/in/paulius-skaisgiris-078467161/), [GitLab](https://gitlab.com/p-skaisgiris), and [GitHub](https://github.com/p-skaisgiris) (I only use GitHub when I work on other people's repositories). Email me at (my first name in lowercase) (squiggle) (the name of this website).

You can access my Curriculum Vitae [here](/pdfs/CV.pdf). Last updated 2024-12-10.

</br>

This site is built on top of [this](https://gitlab.com/maasmath/site). If you couldn't tell, I'm probably a little too fond of dark mode. Oh, and [this](https://branch.climateaction.tech/2020/10/05/10-rules-for-building-a-low-impact-website/) is cute.
